'use strict';

var logPrettyPrint;
var logDecorate;

angular.module('starter')
  .config(function (PROFILE) {
    if (PROFILE === 'dev') {
      logPrettyPrint = false;
      logDecorate = false;
    } else {
      window.onerror = function (errorMsg) {
        window.alert('Error occurred: ' + errorMsg);
        return false;
      };
      logPrettyPrint = false;
      logDecorate = true;
    }
  })
  .config(function ($logProvider) {
    $logProvider.debugEnabled(false);
  })
  .config(function ($provide) {
    $provide.decorator('$log', function ($delegate) {

      var enhanceLogger = function (log) {
        /**
         * Partial application to pre-capture a logger function
         */
        var prepareLogFn = function (logFn, tag) {
          /**
           * Invoke the specified `logFn<` with the supplant functionality...
           */
          var enhancedLogFn = function () {
            var args = [].slice.call(arguments);

            // Prepend tag
            if (tag !== undefined) {
              args.unshift(tag);
            }

            if (logDecorate && !args[0].stack) {
              for (var i = 0, msg = ''; i < args.length; i++) {
                var arg = args[i];
                if (typeof arg === 'object') {
                  msg += angular.toJson(arg, logPrettyPrint);
                } else {
                  msg += arg;
                }
                msg += ' ';
              }

              // Call the original with the output prepended with formatted timestamp
              // TODO: Bug somewhere here
              logFn.call(null, msg);
            } else {
              logFn.apply(null, args);
            }
          };

          // Special... only needed to support angular-mocks expectations
          enhancedLogFn.logs = [];

          return enhancedLogFn;
        };

        var getInstance = function (tag) {
          tag = ( tag !== undefined ) ? '[' + tag + ']' : '';

          return {
            log: prepareLogFn(log.log, tag),
            info: prepareLogFn(log.info, tag),
            warn: prepareLogFn(log.warn, tag),
            debug: prepareLogFn(log.info, tag), // !!!
            error: prepareLogFn(log.error, tag)
          };
        };

        log.log = prepareLogFn(log.log);
        log.info = prepareLogFn(log.info);
        log.warn = prepareLogFn(log.warn);
        log.debug = prepareLogFn(log.info); // !!!
        log.error = prepareLogFn(log.error);

        log.getInstance = getInstance;

        return log;
      };

      return enhanceLogger($delegate);
    });
  });
