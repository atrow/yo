'use strict';

angular.module('starter')
// Node server
//  .constant('SERVER', 'http://192.168.1.105:8080/')
//  .constant('API', 'http://192.168.1.105:8080/api/')
//  .constant('ROOT', 'http://192.168.1.105:8080/')
//  .constant('SERVER', 'http://192.168.1.105:9090/')
//  .constant('API', 'http://192.168.1.105:9090/api/')
//  .constant('ROOT', 'http://192.168.1.105:9090/')
  .constant('SERVER', 'https://futurelink.xyz/')
  .constant('API', 'https://futurelink.xyz/api/')
  .constant('ROOT', 'https://futurelink.xyz/')
  .constant('PROFILE', 'prod');
  //.constant('PROFILE', 'dev');
