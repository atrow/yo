'use strict';

angular.module('starter')
  .config(function Config($httpProvider, jwtInterceptorProvider) {

    jwtInterceptorProvider.tokenGetter = function () {
      return localStorage.getItem('jwt');
    };
    $httpProvider.interceptors.push('jwtInterceptor');
  });
