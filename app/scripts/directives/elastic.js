'use strict';
// From http://stackoverflow.com/a/24090733

angular.module('starter')
  .directive('elastic',
  function () {
    return {
      restrict: 'A',
      link: function ($scope, element) {
        $scope.initialHeight = $scope.initialHeight || element[0].style.height || '18px';
        element[0].style.height = $scope.initialHeight;
        var resize = function () {
          element[0].style.height = $scope.initialHeight;
          element[0].style.height = 2 + element[0].scrollHeight + 'px';
        };
        element.on('focus blur keyup change', resize);
      }
    };
  }
);
