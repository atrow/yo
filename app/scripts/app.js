// Ionic Starter App
'use strict';

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'ngCordova', 'ngResource', 'ngSnowflake', 'ja.qr', 'angular-jwt', 'ngStorage'])

.run(function($timeout, $ionicPlatform, AuthService, LoginModalService) {
  $ionicPlatform.ready(function() {

    $timeout(function () {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
        window.cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    }, 500);

    if (!AuthService.getUser()) {
      LoginModalService.launchModal();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.list', {
      url: '/list/:folder',
      views: {
          'menuContent': {
              templateUrl: 'templates/list.html',
              controller: 'ListCtrl'
          }
      }
  })

  .state('app.item', {
      url: '/items/:id',
      views: {
          'menuContent': {
              templateUrl: 'templates/item.html',
              controller: 'ItemCtrl'
          }
      }
  });
  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/list/sent');
});
