'use strict';

angular.module('starter')

  .controller('ItemCtrl', function ListCtrl($rootScope, $scope, $ionicViewService, $log, $q, $stateParams, $ionicPopover,
                                            $ionicPopup, $cordovaSocialSharing, $cordovaContacts,
                                            EditItemModalService, ItemService, QrModalService) {

    $log = $log.getInstance('ItemCtrl');

    $scope.id = $stateParams.id;

    var showButtons = function (folder) {

      $scope.showEdit = $scope.showArchive = $scope.showRestore = $scope.showTrash = $scope.showDelete = false;

      switch (folder) {

        case 'sent':
          $scope.showEdit = true;
          $scope.showArchive = true;
          $scope.showTrash = true;
          break;

        case 'received':
          $scope.showArchive = true;
          $scope.showTrash = true;
          break;

        case 'drafts':
          $scope.showEdit = true;
          $scope.showRestore = true;
          $scope.showTrash = true;
          break;

        case 'archive':
          $scope.showEdit = true;
          $scope.showRestore = true;
          $scope.showTrash = true;
          break;

        case 'trash':
          $scope.showEdit = true;
          $scope.showRestore = true;
          $scope.showDelete = true;
          break;

      }
    };

    $rootScope.$on('item-updated', function (event, data) {
      $scope.item = data;
      showButtons($scope.item.folder);
      $scope.$broadcast('scroll.refreshComplete');
    });

    $rootScope.$on('global-items-update', function () {
      $scope.onRefresh();
    });

    ItemService.getFolders();

    var showItem = function (id) {
      ItemService.getItem({id: id});
    };

    $ionicPopover.fromTemplateUrl('adhoc/popover.html', {
      scope: $scope
    }).then(function (popover) {
      $scope.popover = popover;
    });

    $scope.onRefresh = function () {
      $log.info('Refreshing...');
      showItem($scope.id);
    };

    $scope.onQr = function () {
      QrModalService.launchModal($scope.item);
    };

    $scope.onShare = function (item) {
      try {
        $cordovaSocialSharing.share(item.title, item.title, null, item.url);
      } catch (error) {
        var msg = 'Social sharing not available';
        window.alert(msg);
        $log.warn(msg, error);
      }
    };

    var onPublish = function (item) {
      $log.info('Saving item:', item);
      $scope.item = {};
      _.merge($scope.item, item); // jshint ignore:line
      ItemService.save(item);//ItemService.saveAsSent(item);
    };

    $scope.onEdit = function (item) {
      $log.info('Editing item:', item);
      $scope.popover.hide();
      $scope.editItem = {};
      _.merge($scope.editItem, item); // jshint ignore:line
      EditItemModalService.launchModal(
        $scope.editItem,
        onPublish
      );
    };

    var onItemMoved = function () {
      $scope.popover.hide();
      $ionicViewService.getBackView().go();
    };

    $scope.onArchive = function (item) {
      $log.info('Archiving item:', item);
      onItemMoved();
      ItemService.archive(item);
    };

    $scope.onRestore = function (item) {
      $log.info('Restoring item:', item);
      onItemMoved();
      ItemService.restore(item);
    };

    $scope.onTrash = function (item) {
      $log.info('Trashing item:', item);
      onItemMoved();
      ItemService.trash(item);
    };

    $scope.onDelete = function (item) {
      $log.info('Deleting item:', item);
      onItemMoved();
      ItemService.erase(item);
    };

    $scope.onRefresh();
  });
