'use strict';

angular.module('starter')

  .controller('EditItemCtrl', function ListCtrl($scope, $log, $ionicPopup, $cordovaSocialSharing, $cordovaContacts, QrModalService) {

    $log = $log.getInstance('EditItemCtrl');

    $scope.onQr = function (item) {
      QrModalService.launchModal(item);
    };

    $scope.onShare = function (item) {
      try {
        $cordovaSocialSharing.share(item.title, item.title, null, item.url);
      } catch (error) {
        var msg = 'Social sharing not available';
        window.alert(msg);
        $log.warn(msg, error);
      }
    };

    $scope.onDeleteEmail = function (item, index) {
      $log.info('Deleting email from item:', item, index);
      item.recipients.splice(index, 1);
    };

    $scope.onDeleteLink = function (item, index) {
      $log.info('Deleting link from item:', item, index);
      item.links.splice(index, 1);
    };

    var addEmail = function (item, email) {
      if (!item.recipients) {
        item.recipients = [];
      }
      if (_.contains(item.recipients, email)) { // jshint ignore:line
        $log.warn('Already added email:', email);
        return;
      }
      item.recipients.push(email);
    };

    var addLink = function (item, url) {
      if (!item.links) {
        item.links = [];
      }
      for (var i = 0; i < item.links.length; i++) {
        if (item.links[i].url === url) {
          $log.warn('Already added link:', url);
          return;
        }
      }
      item.links.push({url: url});
    };

    $scope.onAddContact = function (item) {
      $log.info('Adding Contact');
      if (!$cordovaContacts) {
        window.alert('Error');
        return;
      }

      $log.info('Searching for contacts');
      $cordovaContacts.pickContact()
        .then(function (contact) {
          $log.info('CONTACT:', contact);
          if (contact.emails && contact.emails.length > 0 && contact.emails[0].value) {
            addEmail(item, contact.emails[0].value);
          } else {
            window.alert('Contact has no email');
          }
        }, function (error) {
          throw error;
        });
    };

    $scope.onEnterEmail = function (item) {

      $scope.inputData = {};

      $ionicPopup.show({
        template: '<input type="email" ng-model="inputData.email">',
        title: 'Enter Email',
        //subTitle: 'Please use normal things',
        scope: $scope,
        buttons: [
          {text: 'Cancel'},
          {
            text: 'OK',
            type: 'button-positive',
            onTap: function (event) {
              if (!$scope.inputData.email) {
                //don't allow the user to close unless input is valid
                event.preventDefault();
              } else {
                addEmail(item, $scope.inputData.email);
              }
            }
          }
        ]
      })
        .then(function (result) {
          $log.info('Enter email finished', result);
        });
    };

    $scope.onAddLink = function (item) {

      $scope.inputData = {};

      $ionicPopup.show({
        template: '<input type="url" ng-model="inputData.url">',
        title: 'Enter URL',
        //subTitle: 'Please use normal things',
        scope: $scope,
        buttons: [
          {text: 'Cancel'},
          {
            text: 'OK',
            type: 'button-positive',
            onTap: function (event) {
              if (!$scope.inputData.url) {
                //don't allow the user to close unless input is valid
                event.preventDefault();
              } else {
                addLink(item, $scope.inputData.url);
              }
            }
          }
        ]
      })
        .then(function (result) {
          $log.info('Add Link finished', result);
        });
    };

  });
