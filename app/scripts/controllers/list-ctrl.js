'use strict';

angular.module('starter')

  .controller('ListCtrl', function ListCtrl($scope, $timeout, $log, $q, $stateParams, $ionicListDelegate, ItemService) {

    $log = $log.getInstance('ListCtrl');

    var showButtons = function (folder) {

      $scope.showArchive = $scope.showRestore = $scope.showTrash = $scope.showDelete = false;

      switch (folder) {

        case 'sent':
          $scope.showArchive = true;
          $scope.showTrash = true;
          break;

        case 'received':
          $scope.showArchive = true;
          $scope.showTrash = true;
          break;

        case 'drafts':
          $scope.showRestore = true;
          $scope.showTrash = true;
          break;

        case 'archive':
          $scope.showRestore = true;
          $scope.showTrash = true;
          break;

        case 'trash':
          $scope.showRestore = true;
          $scope.showDelete = true;
          break;

      }
    };

    // Fixing Ionic View caching issues
    $scope.$on('$ionicView.enter',
      function(){
        showItemList($scope.folder);
      });

    $scope.folder = $stateParams.folder;
    showButtons($scope.folder);

    $scope.$on('list-updated', function (event, data) {
      $scope.itemList = data;
      $scope.$broadcast('scroll.refreshComplete');
    });

    $scope.$on('global-items-update', function () {
      showItemList($scope.folder);
    });

    var showItemList = function (folder) {
      ItemService.getList(folder);
    };

    $scope.onFolderSelect = function (folder) {
      $log.info('Selected folder:', folder);
      $scope.folder = folder;
      if (folder) {
        showItemList(folder);
      }
    };

    $scope.onRefresh = function () {
      $log.info('Refreshing...');
      ItemService.refresh();
    };

    $scope.onArchive = function (item) {
      $log.info('Archiving item:', item);
      $ionicListDelegate.closeOptionButtons();
      ItemService.archive(item);
    };

    $scope.onRestore = function (item) {
      $log.info('Restoring item:', item);
      $ionicListDelegate.closeOptionButtons();
      ItemService.restore(item);
    };

    $scope.onTrash = function (item) {
      $log.info('Trashing item:', item);
      $ionicListDelegate.closeOptionButtons();
      ItemService.trash(item);
    };

    $scope.onDelete = function (item) {
      $log.info('Deleting item:', item);
      $ionicListDelegate.closeOptionButtons();
      ItemService.erase(item);
    };

  });
