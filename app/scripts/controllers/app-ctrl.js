'use strict';

angular.module('starter')

  .controller('AppCtrl', function ($rootScope, $scope, $log, $ionicPopover, $ionicPopup, $timeout,
                                   ItemService, EditItemModalService, DbService, AuthService, LoginModalService) {

    $log = $log.getInstance('AppCtrl');

    // Perform the login action when the user submits the login form
    $scope.doLogout = function () {
      $log.info('Logging out');

      $ionicPopup.confirm({
        title: 'Are you sure?',
        subTitle: 'Logging out will clean up your application data',
      })
        .then(function () {
          DbService.eraseData()
            .then(function () {
              $rootScope.$broadcast('global-items-update');
              AuthService.eraseUser();
              LoginModalService.launchModal();
            });
        });
    };

    $rootScope.$on('folders-updated', function (event, data) {
      $scope.folders = data;
    });

    ItemService.getFolders();

    var isFilled = function (item) {
      return (
      item.title ||
      item.description ||
      (item.recipients && item.recipients.length > 0) ||
      (item.links && item.links.length > 0)
      );
    };

    $scope.onPublish = function (item) {
      $log.info('Saving item:', item);
      ItemService.saveAsSent(item);
    };

    $scope.onDraft = function (item) {
      if (!isFilled(item)) {
        $log.info('Nothing to save as draft');
        return;
      }

      $log.info('Closed edit: Saving draft');
      ItemService.saveAsDraft(item);

      var hint = $ionicPopup.show({
        title: 'Saved as draft',
        buttons: [{
          text: 'OK',
          type: 'button-positive'
        }]
      });
      $timeout(function () {
        hint.close();
      }, 2000);
    };

    $scope.onCreate = function () {

      var item = ItemService.createNew();

      EditItemModalService.launchModal(
        item,
        $scope.onPublish,
        null,
        $scope.onDraft
      );

    };

  });
