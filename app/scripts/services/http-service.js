'use strict';

angular.module('starter')

  .factory('HttpService', function ($rootScope, $log, $q, $timeout, $http, Base64, ListFactory, UserFactory) {

    $log = $log.getInstance('HttpService');

    $http.defaults.headers.common.Accept = 'application/json';

    var service = {};

    var handleError = function (error, status, headers, config, nextHandler) {
      $log.error('HTTP Error:', error, status, headers, config);
      if (error && error.status) {
        switch (error.status) {
          case 401:
            $log.error('HTTP AUTH Error');
            $rootScope.$broadcast('auth-error');
        }
      }
      if (nextHandler) {
        nextHandler(error.data);
      }
    };

    service.fetchDump = function (since) {
      var deferred = $q.defer();
      var response = ListFactory.getDump(
        {since: since},
        null,
        function () {
          $log.info('HTTP: Got items:' + response.length);
          deferred.resolve(response);
        },
        function (error, status, headers, config) {
          handleError(error, status, headers, config, function (data) {
            deferred.reject(data);
          });
        }
      );

      return deferred.promise;
    };

    service.updateItemList = function (list) {
      var deferred = $q.defer();
      var response = ListFactory.updateList(
        list,
        function () {
          $log.info('Got response:', response);
          deferred.resolve(response);
        },
        function (error, status, headers, config) {
          handleError(error, status, headers, config, function (data) {
            deferred.reject(data);
          });
        }
      );

      return deferred.promise;
    };

    service.login = function (user, pass) {
      var deferred = $q.defer();

      $http.defaults.headers.common.Authorization = 'Basic ' + Base64.encode(user + ':' + pass);

      UserFactory.login(
        {},
        null,
        function (data) {
          $log.info('Got response:', data);
          delete $http.defaults.headers.common['Authorization'];  // jshint ignore:line
          deferred.resolve(data);
        },
        function (error, status, headers, config) {
          handleError(error, status, headers, config, function (data) {
            delete $http.defaults.headers.common['Authorization'];  // jshint ignore:line
            deferred.reject(data);
          });
        }
      );

      return deferred.promise;
    };

    service.register = function (name, username, pass, confirm) {
      var deferred = $q.defer();
      delete $http.defaults.headers.common['Authorization'];  // jshint ignore:line

      UserFactory.register(
        {nickname: name, username: username, password: pass, confirm: confirm},
        null,
        function (data) {
          $log.info('Got response:', data);
          deferred.resolve(data);
        },
        function (error, status, headers, config) {
          handleError(error, status, headers, config, function (data) {
            deferred.reject(data);
          });
        }
      );

      return deferred.promise;
    };

    return service;
  });
