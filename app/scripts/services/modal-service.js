'use strict';

angular.module('starter')

  .service('ModalService', function ($ionicModal, $rootScope) {

    var service = {};

    service.init = function (tpl, $scope) {

      $scope = $scope || $rootScope.$new();

      var promise = $ionicModal.fromTemplateUrl(tpl, {
        scope: $scope,
        animation: 'slide-in-up'
      }).then(function (modal) {
        return modal;
      });

      return promise;
    };

    return service;

  });
