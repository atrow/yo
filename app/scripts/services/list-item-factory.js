'use strict';

angular.module('starter')

  .factory('ListFactory', function ($resource, API) {
    return $resource(API + 'list',
      {id: '@id'}, {
        getDump: {method: 'GET', isArray: true, url: API + 'dump'},
        updateList: {method: 'POST', isArray: true}
      });
  })

  .factory('UserFactory', function ($resource, API) {
    return $resource(API, {}, {
        login: {method: 'POST', url: API + 'login'},
        register: {method: 'POST', url: API + 'register'}
      });
  });
