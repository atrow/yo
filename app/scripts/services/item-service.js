'use strict';

angular.module('starter')

  .factory('ItemService', function ItemService($rootScope, $log, $cordovaNetwork, $interval,
                                               HttpService, DbService, CryptService, AuthService, ROOT) {

    $log = $log.getInstance('ItemService');

    var folders = [
      {
        _id: '~folder_sent',
        name: 'sent',
        new: 0,
        total: 0
      },
      {
        _id: '~folder_received',
        name: 'received',
        new: 0,
        total: 0
      },
      {
        _id: '~folder_drafts',
        name: 'drafts',
        new: 0,
        total: 0
      },
      {
        _id: '~folder_archive',
        name: 'archive',
        new: 0,
        total: 0
      },
      {
        _id: '~folder_trash',
        name: 'trash',
        new: 0,
        total: 0
      }
    ];

    var scheduled;

    var service = {
      defaultFolder: 'sent'
    };

    var pushChanges = function () {
      $log.debug('Trying to push changes to Server');

      try {
        if ($cordovaNetwork.isOffline()) {
          $log.info('Device is Offline');
          return;
        }
      } catch (error) {
        $log.error(error);
      }

      DbService.getModifiedItems()
        .then(function (list) {
          $log.debug('Got modified items: ', list.length);
          HttpService.updateItemList(list)
            .then(onItemsPushedResponse, function(error) {
              $log.info('Got HTTP error', error);
              cancelUpdate();
            });
        });
    };

    var pullChanges = function () {
      $log.debug('Trying to pull changes from Server');

      try {
        if ($cordovaNetwork.isOffline()) {
          $log.info('Device is Offline');
          cancelUpdate();
          return;
        }
      } catch (error) {
        $log.error(error);
      }

      DbService.getLastItemUpdate()
        .then(function (time) {
          $log.debug('Getting items dump updated since: ', time);
          HttpService.fetchDump(time)
            .then(onDumpFetch, function(error) {
              $log.info('Got HTTP error', error);
              cancelUpdate();
            });
        });
    };

    var scheduleUpdate = function () {
      $log.info('Scheduled HTTP updates');
      scheduled = $interval(pullChanges, 120000);
    };

    var cancelUpdate = function () {
      $log.info('Suspending updates');
      $interval.cancel(scheduled);
    };

    var refresh = function () {
      pullChanges();
      cancelUpdate();
      scheduleUpdate();
    };

    $rootScope.$on('authenticated', refresh);

    $rootScope.$on('global-items-update', function() {
      DbService.getFolderCounts()
        .then(function (data) {
          for (var i = 0; i < folders.length; i++) {
            if (data[folders[i].name]) {
              folders[i].total = data[folders[i].name];
            } else {
              folders[i].total = 0;
            }
          }
          onFoldersUpdate(folders);
        });
    });

    $rootScope.$on('$cordovaNetwork:online',
      function () {
        scheduleUpdate();
    });

    $rootScope.$on('$cordovaNetwork:offline',
      function () {
        cancelUpdate();
    });

/*
    var onHttpError = function (error) {
      ErrorService.httpError(error);
    };
*/

    var onFoldersUpdate = function (data) {
      $log.debug('Folders updated, total: ', data.length);
      $rootScope.$broadcast('folders-updated', data);
    };

    var updateUrls = function (items) {
      for (var i = 0; i < items.length; i++) {
        items[i].url = ROOT + items[i].id;
      }
    };

    var onDumpFetch = function (data) {
      $log.debug('Fetched items dump by HTTP, total: ', data.length);
      updateUrls(data);
      DbService.applyDump(data)
        .then(function () {
          $rootScope.$broadcast('global-items-update');
        });
    };

    var onItemsPushedResponse = function (data) {
      $log.debug('Fetched successfully pushed items by HTTP, total: ', data.length);
      DbService.updatePushedItems(data)
        .then(function () {
          $rootScope.$broadcast('global-items-update');
        });
    };

    var onItemUpdate = function (data) {
      $log.debug('Item updated: ', data);
      $rootScope.$broadcast('item-updated', data);
    };

    var onListUpdate = function (data) {
      $log.debug('List updated, total: ', data.length);
      $rootScope.$broadcast('list-updated', data);
    };


    DbService.initialize();
    pullChanges();
    scheduleUpdate();

    service.refresh = refresh;

    service.getFolders = function () {
      onFoldersUpdate(folders);
    };

    service.getList = function (folder) {
      if (!folder) {
        folder = service.defaultFolder;
      }

      DbService.getItemsByFolder(folder)
        .then(onListUpdate);
    };

    var save = function (item) {
      $log.info('Saving item with id:', item.id);

      DbService.updateItem(item)
        .then(pushChanges)
        .then(function () {
          $rootScope.$broadcast('global-items-update');
        });
    };

    service.archive = function (item) {
      item.folder = 'archive';
      save(item);
    };

    service.restore = function (item) {
      if (item.owned) {
        item.folder = 'sent';
      } else {
        item.folder = 'received';
      }
      save(item);
    };

    service.trash = function (item) {
      item.folder = 'trash';
      save(item);
    };

    service.erase = function (item) {
      item.folder = 'deleted';
      save(item);
    };

    service.getItem = function (item) {
      if (!(item && item.id)) {
        $log.error('No id for item request');
        return;
      }

      DbService.getItem(item.id)
        .then(onItemUpdate);
    };

    service.saveAsDraft = function (item) {
      item.folder = 'drafts';
      save(item);
    };

    service.saveAsSent = function (item) {
      item.folder = 'sent';
      save(item);
    };

    service.save = save;

    service.remove = function (item) {
      $log.info('Deleting item with id:', item.id);

      return service.groupAction('delete', [item.id]);
    };

    service.createNew = function () {
      var id = CryptService.nextId();
      $log.info('Creating item with id:', id);

      return {
        id: id,
        folder: 'drafts',
        url: ROOT + id,
        from: AuthService.getUser().username,
        owned: true
      };
    };

    return service;
  });
