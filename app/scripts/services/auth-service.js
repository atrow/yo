'use strict';

angular.module('starter')

  .factory('AuthService', function ($rootScope, $q, $log, $localStorage, $ionicPopup, jwtHelper, HttpService, SERVER) {

    $log = $log.getInstance('AuthService');

    var service = {};

    service.getUser = function () {
      $log.debug('Getting current user');

      var user = $localStorage.user;
      $log.info('Current user:', user);

      return user;
    };

    service.eraseUser = function () {
      $log.debug('Deleting current user');

      delete $localStorage.user;
      localStorage.removeItem('jwt');
    };

    var applyUser = function (username, name, token) {
      $localStorage.user = {
        username: username,
        name: name,
        token: token
      };
      localStorage.setItem('jwt', token);
    };

    var parseToken = function (params) {
      return params.split('=')[1];
    };

    var openSocialLogin = function (provider) {
      var deferred = $q.defer();
      var outer = $q.defer();

      var win = window.open(SERVER + 'api/signin/' + provider, '_blank', 'location=no,');

      win.addEventListener('loadstop', function (e) {
        var parser = document.createElement('a');

        console.log('Inappbrowser Load stopped at: ' + e.url);
        parser.href = e.url;

        console.log('Parser hostname: ' + parser.hostname);
        if (parser.hostname === 'localhost') {
          console.log('Parser pathname:' + parser.pathname);
          if (parser.pathname === '/success') {
            var token = parseToken(parser.search);
            console.log('Parser token:', token);
            deferred.resolve(token);
          } else if (parser.pathname === '/error') {
            deferred.reject();
          }
        }
      });

      deferred.promise
        .then(function (result) {
          outer.resolve(result);
          win.close();
        })
        .catch(function () {
          win.close();
        });

      return outer.promise;
    };

    service.login = function (user, pass) {
      $log.info('Logging in as:', user);
      var deferred = $q.defer();

      HttpService.login(user, pass)
        .then(function (data) {
          applyUser(data.username, data.name, data.token);
          $rootScope.$broadcast('authenticated', data.username);
          deferred.resolve(true);
        })
        .catch(function (error) {
          $log.error(error);

          $ionicPopup.show({
            title: 'Login failed',
            subTitle: error.status + ': ' + error.error,
            buttons: [{
              text: 'OK',
              type: 'button-positive'
            }]
          })
            .then(function () {
              deferred.reject();
            });
        });

      return deferred.promise;
    };

    service.register = function (name, user, pass, confirm) {
      $log.info('Registering in as:', user);
      var deferred = $q.defer();

      if (pass !== confirm) {
        $ionicPopup.show({
          title: 'Passwords does not match',
          subTitle: 'Re-enter passwords',
          buttons: [{
            text: 'OK',
            type: 'button-positive'
          }]
        })
          .then(function () {
            deferred.reject();
          });
        return;
      }

      HttpService.register(name, user, pass, confirm)
        .then(function () {

          $ionicPopup.show({
            title: 'Registration successful',
            subTitle: 'You need to confirm your email address.<br>Please, check your inbox.',
            buttons: [{
              text: 'OK',
              type: 'button-positive'
            }]
          })
            .then(function () {
              deferred.resolve(true);
            });
        })
        .catch(function (error) {
          $log.error(error);

          $ionicPopup.show({
            title: 'Registration failed',
            subTitle: error.status + ':' + error.message,
            buttons: [{
              text: 'OK',
              type: 'button-positive'
            }]
          })
            .then(function () {
              deferred.reject();
            });
        });

      return deferred.promise;
    };

    service.socialLogin = function (provider) {
      $log.info('Launching Social authentication:');
      var deferred = $q.defer();

      openSocialLogin(provider)
        .then(function (payload) {
          $log.info('Got raw token:', payload);

          var token = jwtHelper.decodeToken(payload);
          $log.info('Decoded:', token);

          applyUser(token.sub, token.name, payload);
          $rootScope.$broadcast('authenticated', token.sub);
          deferred.resolve();
        })
        .catch(function () {
          deferred.reject();
        });

      return deferred.promise;
    };

    return service;
  });
