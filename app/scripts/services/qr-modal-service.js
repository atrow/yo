'use strict';

angular.module('starter')

  .service('QrModalService', function ($rootScope, $log, ModalService) {

    var service = {};

    $log = $log.getInstance('QrModalService');

    var $scope = $rootScope.$new();

    var modalId = 'QrModalServiceModal';

    service.launchModal = function (item) {
      $scope.item = item;

      ModalService
        .init('templates/qr.html', $scope)
        .then(function (modal) {
          $scope.modal = modal;
          $scope.modal.id = modalId;

          var unregister = $scope.$on('modal.hidden', function (event, modal) {
            if (modal.id === modalId) {
              unregister();
              $scope.modal.remove();
            }
          });

          $scope.modal.show();
        });

    };

    return service;
  });
