'use strict';

angular.module('starter')

  .factory('DbService', function DbService($rootScope, $q, $log, $window, ErrorService) {

    $log = $log.getInstance('DbService');
    var db;
    var dbName = 'promise.db';

    var service = {};

    service.initialize = function () {

      var waits = [];

      if ($window.sqlitePlugin) {
        $log.info('Using PLATFORM SQLite');
      } else {
        $log.warn('Using WEBKIT WEBSQL');
      }
      db = new PouchDB(dbName); // jshint ignore:line

      if (!db) {
        $log.error('Failed to open DB');
        ErrorService.error('Failed to open DB');
      }

      // create a design doc
      var ddoc = {
        _id: '_design/index',
        views: {
          byFolder: {
            map: function mapFun(doc) {
              if (doc.folder) {
                emit(doc.folder); // jshint ignore:line
              }
            }.toString()
          },
          byModifiedTrue: {
            map: function mapFun(doc) {
              if (doc.modified) {
                emit(doc.modified); // jshint ignore:line
              }
            }.toString()
          },
          byId: {
            map: function mapFun(doc) {
              if (doc.id) {
                emit(doc.id); // jshint ignore:line
              }
            }.toString()
          },
          lastUpdate: {
            map: function mapFun(doc) {
              if (doc.updated) {
                emit(null, doc.updated); // jshint ignore:line
              }
            }.toString(),
            reduce: '_stats'
          },
          user: {
            map: function mapFun(doc) {
              if (doc.username) {
                emit(doc.username); // jshint ignore:line
              }
            }.toString()
          }
        }
      };

      var waitDoc = $q.defer();
      waits.push(waitDoc.promise);

      // save the design doc
      $q.when(db.put(ddoc))
        .then(function (result) {
          $log.info('Created Design doc:', result);
        })
        .catch(function (error) {
          // ignore if doc already exists
          if (error.status !== 409) {
            throw error;
          }
          $log.info('Design Doc exists');
        })
        .finally(function () {
          waitDoc.resolve();
        });

      var waitCompact = $q.defer();
      waits.push(waitCompact.promise);

      $q.when(db.compact())
        .then(function (result) {
          $log.info('Compacted DB:', result);
        })
        .catch(function (err) {
          $log.error('DB Error:', err);
        })
        .finally(function () {
          waitCompact.resolve();
        });

      return $q.all(waits);
    };

    var putItem = function (item) {
      $log.debug('Saving item: ', item);
      var deferred = $q.defer();

      $q.when(db.put(item))
        .then(function (item) {
          $log.debug('Done Saving item: ', item);
          deferred.resolve(item);
        })
        .catch(function (error) {
          throw error;
        });

      return deferred.promise;
    };

    var getItem = function (id) {
      $log.debug('Getting item by id: ', id);
      var deferred = $q.defer();

      $q.when(db.query('index/byId', {
        key: id,
        'include_docs': true
      }))
        .then(function (docs) {
          var items = docs.rows.map(function (row) {
            return row.doc;
          });
          if (items.length === 1) {
            $log.debug('Got item: ', items[0]);
            deferred.resolve(items[0]);
          } else {
            $log.error('Item not found:', items);
            deferred.resolve(null);
          }
        })
        .catch(function (error) {
          // ignore if not found
          if (error.status !== 404) {
            throw error;
          }
          $log.info('Item not found by id', id);
          deferred.resolve(null);
        });

      return deferred.promise;
    };

    var mergeItem = function (doc, item) {
      if (doc) {
        _.assign(doc, item); // jshint ignore:line
        return doc;
      } else {
        doc = {_id: item.id};
        _.assign(doc, item); // jshint ignore:line
        return doc;
      }
    };

    var fetchAndUpdate = function (original) {
      $log.debug('Fetching original to update for: ', original);
      var deferred = $q.defer();

      getItem(original.id)
        .then(function (retrieved) {
          var merged = mergeItem(retrieved, original);
          putItem(merged)
            .then(function (data) {
              $log.debug('Item persisted: ', data);
              deferred.resolve(data);
            })
            .catch(function (error) {
              throw error;
            });
        })
        .catch(function (error) {
          throw error;
        });

      return deferred.promise;
    };

    service.getItem = getItem;

    service.getUser = function () {
      $log.info('Getting user account data');

      var deferred = $q.defer();

      $q.when(db.query('index/user', {
        'include_docs': true
      }))
        .then(function (docs) {
          var items = docs.rows.map(function (row) {
            return row.doc;
          });
          if (items.length === 1) {
            deferred.resolve(items[0]);
          } else {
            $log.error('User not found:', items);
            deferred.resolve(null);
          }
        })
        .catch(function (error) {
          // ignore if not found
          if (error.status !== 404) {
            throw error;
          }
          $log.error('User not found');
          deferred.resolve(null);
        });

      return deferred.promise;
    };

    service.updateItem = function (item) {
      $log.info('Item is modified, updating', item);
      item.modified = true;

      return fetchAndUpdate(item);
    };

    service.applyDump = function (items) {
      $log.info('Batch updating items, total: ', items.length);
      var waits = [];

      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        if (item.modified) {
          $log.warn('Skipping dump update for modified item:', item);
        } else {
          var promise = fetchAndUpdate(item);
          waits.push(promise);
        }

      }
      return $q.all(waits);
    };

    service.updatePushedItems = function (items) {
      $log.info('Updating items, successfully pushed to server, total: ', items.length);
      var waits = [];

      for (var i = 0; i < items.length; i++) {
        var item = items[i];
        item.modified = false;
        var promise = fetchAndUpdate(item);
        waits.push(promise);

      }
      return $q.all(waits);
    };

    // Modified on client
    service.getModifiedItems = function () {
      $log.info('Getting modified items');
      var deferred = $q.defer();

      $q.when(db.query('index/byModifiedTrue', {
        'include_docs': true
      }))
        .then(function (docs) {
          var items = docs.rows.map(function (row) {
            return row.doc;
          });
          deferred.resolve(items);
        })
        .catch(function (error) {
          // ignore if not found
          if (error.status !== 404) {
            throw error;
          }
          $log.info('Modified items Not found');
          deferred.resolve([]);
        });

      return deferred.promise;
    };

    service.getItemsByFolder = function (folder) {
      $log.info('Getting items by folder: ', folder);
      var deferred = $q.defer();

      $q.when(db.query('index/byFolder', {
        key: folder,
        'include_docs': true
      }))
        .then(function (docs) {
          var items = docs.rows.map(function (row) {
            return row.doc;
          });
          deferred.resolve(items);
        })
        .catch(function (error) {
          // ignore if not found
          if (error.status !== 404) {
            throw error;
          }
          $log.info('Empty folder', folder);
          deferred.resolve([]);
        });

      return deferred.promise;
    };

    service.getFolderCounts = function () {
      $log.info('Getting items count per folder');
      var deferred = $q.defer();

      $q.when(db.query('index/byFolder', {
      }))
        .then(function (docs) {
          var folders = {};
          docs.rows.map(function (row) {
            if (!folders[row.key]) {
              folders[row.key] = 0;
            }
            folders[row.key]++;
          });
          deferred.resolve(folders);
        })
        .catch(function (error) {
          // ignore if not found
          if (error.status !== 404) {
            throw error;
          }
          deferred.resolve({});
        });

      return deferred.promise;
    };

    service.getLastItemUpdate = function () {
      $log.info('Getting last update timestamp');
      var deferred = $q.defer();

      $q.when(db.query('index/lastUpdate', {
        reduce: true
      }))
        .then(function (docs) {
          if (docs.rows.length === 1 && docs.rows[0].value) {
            var value = docs.rows[0].value.max;
            deferred.resolve(value);
          } else {
            deferred.resolve(0);
          }
        })
        .catch(function (error) {
          // ignore if not found
          if (error.status !== 404) {
            throw error;
          }
          $log.info('No Last Update data');
          deferred.resolve(0);
        });

      return deferred.promise;
    };

    service.eraseData = function () {
      $log.info('Erasing database');
      var deferred = $q.defer();

      $q.when(db.destroy())
        .then(function () {
          service.initialize()
            .then(function () {
              deferred.resolve();
            });
        })
        .catch(function (error) {
          $log.error('Failed to destroy DB');
          deferred.reject(error);
        });

      return deferred.promise;
    };

    return service;
  });
