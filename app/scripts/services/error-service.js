'use strict';

angular.module('starter')

.service('ErrorService', function ($ionicModal, $log, $rootScope) {
    var service = {};

    var init = function(msg) {

        var promise;
        var $scope = $rootScope.$new();

        $scope.error = msg;

        promise = $ionicModal.fromTemplateUrl('templates/error.html', {
            scope: $scope,
            animation: 'slide-in-up'
        }).then(function(modal) {
            $scope.modal = modal;
            return modal;
        });

        $scope.openModal = function() {
            $scope.modal.show();
        };
        $scope.closeModal = function() {
            $scope.modal.hide();
        };
        $scope.$on('$destroy', function() {
            $scope.modal.remove();
        });

        return promise;
    };

    service.error = function error(msg) {
        $log.error(msg);

        init(msg)
            .then(function(modal) {
                modal.show();
            });
    };

    service.httpError = function httpError(error) {
        var msg = 'HTTP Error';
        if (error && error.status) {
            msg += ': ' + error.status + ' ' + error.statusText;
        }
        if (error && error.data && error.data.message) {
            msg += ': ' + error.data.message;
        }
        service.error(msg);
    };

    return service;
});
