'use strict';

angular.module('starter')

  .factory('CryptService', function CryptService($rootScope, $log, snowflakeService) {

    $log = $log.getInstance('CryptService');
    var service = {};

    service.nextId = function nextId() {
      var id = snowflakeService.nextId();
      $log.info('New id:', id);

      return id;
    };

    return service;
  });
