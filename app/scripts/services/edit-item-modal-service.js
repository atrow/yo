'use strict';

angular.module('starter')

  .service('EditItemModalService', function ($rootScope, $log, $cordovaSocialSharing, $cordovaContacts, ModalService) {

    var service = {};

    $log = $log.getInstance('EditItemModalService');

    var $scope = $rootScope.$new();

    var modalId = 'EditItemServiceModal';

    ModalService
      .init('templates/edit.html', $scope)
      .then(function (modal) {
        $scope.modal = modal;
        $scope.modal.id = modalId;
      });

    service.launchModal = function (item, onPublish, onCancel, onClose) {
      $scope.item = item;
      $scope.modal.show();

      $scope.onPublish = function() {
        $scope.isHidingAfterAction = true;
        $scope.modal.hide();
        onPublish($scope.item);
      };

      $scope.onCancel = function() {
        $scope.isHidingAfterAction = true;
        $scope.modal.hide();
        if (onCancel) {
          onCancel($scope.item);
        }
      };

      var unregister = $scope.$on('modal.hidden', function (event, modal) {
        if (modal.id !== modalId) {
          return;
        }

        unregister();
        if ($scope.isHidingAfterAction) {
          $scope.isHidingAfterAction = false;
        } else {
          if (onClose) {
            onClose($scope.item);
          }
        }
      });

    };

    return service;
  });
