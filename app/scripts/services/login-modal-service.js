'use strict';

angular.module('starter')

  .service('LoginModalService', function ($rootScope, $log, $ionicModal, AuthService) {

    var service = {};

    $log = $log.getInstance('LoginModalService');

    var $scope = $rootScope.$new();
    $scope.loginData = {};

    $scope.switchToLogin = function() {
      $scope.showLogin = true;
      $scope.showRegister = false;
      $scope.title = 'Login';
    };
    $scope.switchToLogin();

    $scope.switchToRegister = function() {
      $scope.showLogin = false;
      $scope.showRegister = true;
      $scope.title = 'Register';
    };

    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope,
      animation: 'slide-in-up',
      backdropClickToClose: false,
      hardwareBackButtonClose: false
    }).then(function (modal) {
      $scope.modal = modal;
    });

    service.launchModal = function () {
      if ($scope.modal.isShown()) {
        return;
      }

      $scope.modal.show();

      $scope.onLogin = function (username, password) {
        AuthService.login(username, password)
          .then(function () {
            $scope.loginData.password = null;
            $scope.modal.hide();
          });
      };

      $scope.onRegister = function (name, username, password, confirm) {
        AuthService.register(name, username, password, confirm)
          .then(function () {
            $scope.loginData = {username: $scope.loginData.username};
            $scope.switchToLogin();
          });
      };

      $scope.onSocialLogin = function (provider) {
        AuthService.socialLogin(provider)
          .then(function () {
            $scope.modal.hide();
          });
      };

      $scope.onCancel = function () {
        $scope.modal.hide();
      };

    };

    $rootScope.$on('auth-error',
      function () {
        service.launchModal();
      });

    return service;
  });
