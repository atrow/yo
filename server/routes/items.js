var items = [
  {
    "id": "613687324328464384",
    "folder": "sent",
    "from": "Серг",
    "title": "Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.",
    "description": "А помнишь, ты мне обещал?",
    "owned": true,
    "url": "http://192.168.1.105:5000/613687324328464384",
    "created": 1435149434767,
    "updated": 1435149434767,
    "recipients": ["go@go.com"],
    "links": [
      {
        "name": "Photos",
        "url": "http://stackoverflow.com/questions/7952448/accessing-partial-response-using-ajax-or-websockets"
      },
      {
        "name": "http://ionicons.com/",
        "url": "http://ionicons.com/"
      }
    ],
    "new": true
  },
  {
    "id": "613687324328464385",
    "folder": "sent",
    "from": "Олег",
    "title": "Огого",
    "description": null,
    "owned": true,
    "url": "http://192.168.1.105:5000/613687324328464385",
    "created": 1435149438767,
    "updated": 1435149434767,
    "recipients": [],
    "links": [],
    "new": false
  },
  {
    "id": "613687324328464386",
    "folder": "received",
    "from": "Hoho@ho.com",
    "title": "Hoho ohoho",
    "description": null,
    "url": "http://192.168.1.105:5000/613687324328464386",
    "created": 1435149438767,
    "updated": 1435149434767,
    "recipients": [],
    "links": [{
      "name": "Photos",
      "url": "http://stackoverflow.com/questions/7952448/accessing-partial-response-using-ajax-or-websockets"
    }],
    "new": false
  },
  {
    "id": "613687324328464387",
    "folder": "archive",
    "from": "Ahhho@ho.com",
    "title": "Alala",
    "description": null,
    "url": "http://192.168.1.105:5000/613687324328464387",
    "created": 1435149438767,
    "updated": 1435149434767,
    "recipients": [],
    "links": [],
    "new": false
  },
  {
    "id": "613687324328464388",
    "folder": "trash",
    "from": "Trasssh",
    "title": "trash",
    "description": null,
    "owned": true,
    "url": "http://192.168.1.105:5000/613687324328464388",
    "created": 1435149438767,
    "updated": 1435149439767,
    "recipients": ["go@go.com"],
    "links": [],
    "new": false
  }
];

var folders = [
  {
    "name": "sent",
    "new": 1,
    "total": 1
  },
  {
    "name": "received",
    "new": 1,
    "total": 1
  },
  {
    "name": "drafts",
    "new": 0,
    "total": 0
  },
  {
    "name": "archive",
    "new": 0,
    "total": 0
  },
  {
    "name": "trash",
    "new": 0,
    "total": 1
  }
];

var pages = {
  "current": 0,
  "from": 1,
  "to": 1,
  "total": 1
};

exports.getList = function (req, res, next) {
  res.send(items);
};

exports.getFolders = function (req, res, next) {
  res.send(folders);
};

exports.getPages = function (req, res, next) {
  res.send(pages);
};

exports.getNull = function (req, res, next) {
  res.send(null);
};

exports.getItem = function (req, res, next) {
  var id = req.params.id;
  res.send(items[0]);
};
