var express = require('express'),
    bodyParser      = require('body-parser'),
    methodOverride  = require('method-override'),
    items        = require('./routes/items'),
    app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(methodOverride());      // simulate DELETE and PUT

// CORS (Cross-Origin Resource Sharing) headers to support Cross-site HTTP requests
app.all('*', function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization');
    next();
});

app.get('/api/list', items.getList);
app.get('/api/dump', items.getList);
app.get('/api/list/folders', items.getFolders);
app.get('/api/list/pages', items.getPages);
app.get('/api/item/:id', items.getItem);
app.post('/api/item/:id', items.getItem);
app.post('/api/list', items.getList);

app.set('port', process.env.PORT || 5000);

app.listen(app.get('port'), function () {
    console.log('Express server listening on port ' + app.get('port'));
});
